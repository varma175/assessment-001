import { NgModule } from '@angular/core';
import { WelcomeRoutingModule } from './welcome-routing.module';
import { UserDetailComponent } from './user-details/user-detail.component';
import { WelcomeComponent } from './welcome.component';
import { DataService } from '../welcome/data.service';

import { NzTableModule } from 'ng-zorro-antd/table';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { FormsModule } from "@angular/forms";
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [WelcomeRoutingModule, NzTableModule, FormsModule, NzButtonModule, NzGridModule, NzIconModule, CommonModule],
  declarations: [WelcomeComponent, UserDetailComponent],
  providers: [DataService],
  exports: [WelcomeComponent, UserDetailComponent]
})
export class WelcomeModule { }
