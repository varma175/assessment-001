import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome.component';
import { UserDetailComponent } from './user-details/user-detail.component';

const routes: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'user-detail', component: UserDetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeRoutingModule { }
