import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import { Router } from '@angular/router';

@Component({
    selector: 'user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.less']
})

export class UserDetailComponent implements OnInit {
    userData: any;
    constructor(
        public _dataService: DataService,
        public _router: Router) {
    }

    ngOnInit() {
        this.userData = this._dataService.userData;
        if (this.userData == undefined) {
            this._router.navigate(['welcome']);
        }
    }

}
