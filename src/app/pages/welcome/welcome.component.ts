import { Component, OnDestroy, OnInit } from '@angular/core';
import { apiService } from '../../sevices/api.service';
import { DataService } from './data.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
export interface UserDataInterface {
  name: string;
  age: number;
  Phone: number;
  email: string;
  picture: string;
}
@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.less']
})
export class WelcomeComponent implements OnInit, OnDestroy {
  SearchName?: string;
  isTable: boolean;
  userData: any;
  subscription: Subscription;
  constructor(
    public _apiService: apiService,
    public _dataService: DataService,
    public router: Router,
    private route: ActivatedRoute) {
    this.SearchName = '';
    this.userData = {};
    this.isTable = true;
  }

  listOfData: UserDataInterface[] = [];

  ngOnInit(): void {
    this.fetchUsers();
  }

  public fetchUsers(): void {
    this._apiService.getUsers().subscribe(res => {
      let userRes = res.results;
      this.listOfData = userRes;
      this.userData = userRes;
    }, error => {
      console.log(error);
    });
  }

  ngOnDestroy() {
    this._dataService.userData = this.userData;
  }

  onNameChange(event) {
    let filteredItems = this.userData.filter(item => `${item.name.first} ${item.name.last}`.includes(event));
    this.listOfData = filteredItems;
  }

  changeView(viewType) {
    if (viewType == 'table') {
      this.isTable = true;
    } else {
      this.isTable = false;
    }
  }

  getUser(userData) {
    this.userData = userData;
    this.router.navigate(['user-detail'], { relativeTo: this.route });
  }

}
