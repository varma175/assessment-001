import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
@Injectable({ providedIn: 'root' })
export class apiService {
    private subject = new Subject<any>();

    constructor(
        private http: HttpClient) { }

    getUsers(): Observable<any> {
        const usersUrl = 'https://randomuser.me/api/?seed=mindspaceDemo&inc=gender,name,email,phone,picture,location,dob,&results=100&&page=0';

        return this.http.get(usersUrl);
    }
}
